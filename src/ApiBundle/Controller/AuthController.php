<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 30/08/2018
 * Time: 10:36
 */

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class AuthController extends Controller
{
    // User Current
    /**
     * @Rest\View()
     * @Rest\Get("/api/profile")
     * @ApiDoc(
     *  resource=true,
     *  description="Profil de l'utilisateur"
     * )
     */
    public function getProfile() {

        $user = $this->getUser();
        return $user;

    }

    // Modification profile
    /**
     * @Rest\View()
     * @Rest\Put("/api/profile")
     * @ApiDoc(
     *  resource=true,
     *  description="Modification profil de l'utilisateur"
     * )
     */
    public function setProfile(Request $request)
    {
        $user = $this->getUser();

        if ($request->get('date_naissance'))
            $user->setDateNaissance($request->get('date_naissance'));

        if ($request->get('adresse'))
            $user->setAdresse($request->get('adresse'));

        if ($request->get('is_dakar'))
            $user->setIsDakar($request->get('is_dakar'));

        if ($request->get('is_dakar'))
            $user->setIsDakar($request->get('is_dakar'));

        if ($request->get('nom'))
            $user->setNom($request->get('nom'));

        if ($request->get('prenom'))
            $user->setPrenom($request->get('prenom'));

        if ($request->get('telephone'))
            $user->setTelephone($request->get('telephone'));

        if ($request->get('sexe'))
            $user->setSexe($request->get('sexe'));

        if ($request->get('code_generate'))
            $user->setCodeGenerate($request->get('code_generate'));

        if ($request->get('dateinscription'))
            $user->setDateinscription($request->get('dateinscription'));

        $em = $this->getDoctrine()->getManager() ;

        $em->persist($user);
        $em->flush();

        return $user;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/api/photo/{username}/profile")
     * @ApiDoc(
     *  resource=true,
     *  description="Changer la photo de profil (envoyer l'image en base64)"
     * )
     */
    public function setPhotoProfile(Request $request, $username) {

        $em = $this->getDoctrine()->getManager() ;

        $user = $em->getRepository('AppBundle:User')
            ->findOneBy( ['username' => $username] );

        //upload de l'image de l'article
        $file_name = null;
        $baseurl = null;

        if($request->get('photo') != null)
        {
            $tmp = explode(',', $request->get('photo'));
            $tmp = explode(';', $tmp[0]);
            $tmp = explode("/", $tmp[0]);
            $tmp = $tmp[1];
            //var_dump($tmp);
            $file_name = $this->generateRandomString(10).'.'.$tmp;

            // $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath().'/images/'.$file_name;
            $appPath = $this->container->getParameter('kernel.root_dir');
            $webPath = realpath($appPath . '/../web');

            $data = $img = preg_replace('#data:image/[^;]+;base64,#', '', $request->get('photo'));
            $data = base64_decode($data);

            file_put_contents($webPath.'/images/'.$file_name, $data);

            $user->setPhoto($file_name);

            $em->persist($user);
            $em->flush();
        }



        $formatted = [
            "photo" => $file_name
        ];

        return $formatted ;

    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/change-password", name="user_register")
     * @ApiDoc(
     *  resource=true,
     *  description="Changer mot de passe !"
     * )
     */
    public function changePasswordAction(Request $request)
    {

        $userName = $request->get('username');
        $password = $request->get('current_password');
        $first = $request->get('first');
        $second = $request->get('last');

        if ($first != $second)
        {
            $message = [
                "error" => "Veuillez vérifier les champs renseignés, les deux mots de passe ne sont pas identiques"
            ];

            return $message;
        }

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $userName]);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);

        if (!$isValid) {
            $message = [
                "failed" => "Vérifier le mot de passe actuel. Il n'est pas correct."
            ];
            return $message;
        }

        $user->setPlainPassword($first);

        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);

        $message = [
            "success" => "Votre mot de passe été bien modifié !"
        ];

        return $message;

    }

    /**
     * @Rest\View()
     * @Rest\Post("/forget-password", name="user_forgetpassword")
     * @ApiDoc(
     *  resource=true,
     *  description="Mot de passe oublier !"
     * )
     */
    public function forgetPasswordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $email = $request->get('email');

        $user = $this->getDoctrine()->getManager()
            ->getRepository('AppBundle:User')
            ->findOneBy(['email' => $email]);

        if (!$user) {
            $message = [
                "error" => "Cet utilisateur n'hexiste pas"
            ];

            return $message;
        }

        $passwordgenerate = $this->generateUniqueFileName(10);

        $user->setPlainPassword($passwordgenerate);
        $user->setEnabled(true);

        $em->persist($user);
        $em->flush();

        $success = [
            "success" => "Un mot de passe par défaut vous a été envoyé !"
        ];

        //envoie de mail
        $message = (new \Swift_Message('Mot de passe oublié !'))
            ->setFrom('contact@livindkr.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/password_forget.html.twig',
                    array('user' => $user, 'newpassword' => $passwordgenerate)
                ),
                'text/html'
            )
        ;

        $this->get('mailer')->send($message);

        return $success;

    }

    /**
     * @return string
     */
    private function generateUniqueFileName($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @Rest\Post("/inscription", name="user_inscription")
     * @ApiDoc(
     *  description="Inscription"
     * )
     */
    public function inscriptionAction(Request $request)
    {
        $userName = $request->get('username');
        $password = $request->get('password');
        $email = $request->get('email');
        $prenom = $request->get('prenom');
        $nom = $request->get('nom');
        $sexe = $request->get('sexe');
        $telephone = $request->get('telephone');
        $date_naissance = $request->get('date_naissance');


        $userManager = $this->get('fos_user.user_manager');

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['email' => $email]);

        if($user) {
            $formatted = [
                "message" => "Cet utilisateur existe déjà dans la plateforme."
            ];

            return new JsonResponse($formatted);
        }

        if($request->getMethod() == 'POST')
        {
            $user = $userManager->createUser();
            $user->setEnabled(false);
            $user->setRoles(array("ROLE_USER"));
            $user->setPlainPassword($password);
            $user->setUsername($userName);
            $user->setEmail($email);
            $user->setNom($nom);
            $user->setPrenom($prenom);
            $user->setSexe($sexe);
            $user->setTelephone($telephone);
            $user->setDateNaissance($date_naissance);

            // generate code
            $digits = 4;
            $code_generate = rand(pow(10, $digits-1), pow(10, $digits)-1);
            //$code_generate = gmp_random_range(1000, 10000);
            $user->setCodeGenerate($code_generate);



            //check validation error
            //$validator = $this->get('validator', null, array("Registration"));
            //$errors = $validator->validate($user);
            //dump($errors); die;

            $userManager->updateUser($user);


            // send code generator
            $message = (new \Swift_Message('Salut'))
                ->setFrom('contact@livindkr.com')
                ->setTo($email)
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'Emails/registration.html.twig',
                        array('code' => $code_generate, 'username' => $code_generate)
                    ),
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;


            $this->get('mailer')->send($message);

            $formatted = [
                "username" => $userName,
                "message" => "Félicitation votre inscription à été bien enregistré. Veuillez activer votre compte avec le code qui vous a été envoyé par e-mail."
            ];

            return new JsonResponse($formatted);
        }

        $formatted = [
            "message" => "Une erreur est survenu lors de l'inscription"
        ];
        return new JsonResponse($formatted);
    }


    /**
     * @Rest\View()
     * @Rest\Get("/code")
     * @ApiDoc(
     *  resource=true,
     *  description="Validation code d'inscription"
     * )
     */
    public function validationCode(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $request->get('username')]);

        if($user->getCodeGenerate() == $request->get('code')) {
            $user->setEnabled(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $formatted = [
                "success" => "success"
            ];
            return new JsonResponse($formatted);
        }else {
            $formatted = [
                "failed" => "failed"
            ];
            return new JsonResponse($formatted);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Get("/renvoyer-code")
     * @ApiDoc(
     *  resource=true,
     *  description="Renvoyer validation code d'inscription"
     * )
     */
    public function RenvoyerCode(Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $request->get('username')]);

        // generate code
        $digits = 4;
        $code_generate = rand(pow(10, $digits-1), pow(10, $digits)-1);
        //$code_generate = gmp_random_range(1000, 10000);
        $user->setCodeGenerate($code_generate);

        $em = $this->getDoctrine()->getManager() ;

        $em->persist($user);
        $em->flush();


        // send code generator
        $message = (new \Swift_Message('Salut '+ $user->getUsername()))
            ->setFrom('contact@livindkr.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/registration.html.twig',
                    array('code' => $code_generate, 'username' => $code_generate)
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;


        $this->get('mailer')->send($message);

        $formatted = [
            "username" => $user->getUsername()
        ];

        return new JsonResponse($formatted);
    }



    /**
     * @Rest\View()
     * @Rest\Post("/connnect_fb_google")
     * @ApiDoc(
     *  resource=true,
     *  description="Connexion via facebook or google"
     * )
     */
    public function RsLoginAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');

        if($request->getMethod() == 'POST'){

            $email = $request->get('email'); // json-string
            //$password = $request->request->get('password', null); // json-string

            $user = $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->findOneBy(['email' => $email]);

            if (!$user) {

                $characts = '1234567890';
                $code = '';
                for($i=0;$i < 6;$i++) //10 est le nombre de caractères
                {
                    $code .= substr($characts,rand()%(strlen($characts)),1);
                }

                $prenom = $request->get('firstName');
                $nom = $request->get('lastName');
                $username = $prenom.'_'.$nom.'_'.$code;
                $password = $code;


                $user = $userManager->createUser();
                $user->setEnabled(true);
                $user->setPlainPassword($password);
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setNom($nom);
                $user->setPrenom($prenom);

                $userManager->updateUser($user);


                // send code generator
                $message = (new \Swift_Message('Salut '+ $prenom))
                    ->setFrom('contact@livindkr.com')
                    ->setTo($email)
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/registration_rs.html.twig',
                            array('password' => $code, 'username' => $username)
                        ),
                        'text/html'
                    )
                    /*
                     * If you also want to include a plaintext version of the message
                    ->addPart(
                        $this->renderView(
                            'Emails/registration.txt.twig',
                            array('name' => $name)
                        ),
                        'text/plain'
                    )
                    */
                ;


                $this->get('mailer')->send($message);

                $token = $this->getToken($user);

            }
            else{
                $token = $this->getToken($user);

            }

            $formatted = [
                "token" => $token
            ] ;
            return $formatted ;
        }

    }


    public function getToken(User $user)
    {
        return $this->container->get('lexik_jwt_authentication.encoder')
            ->encode([
                'email' => $user->getEmail(),
                'username' => $user->getUsername(),
                'exp' => $this->getTokenExpiryDateTime(),
            ]);
    }

    private function getTokenExpiryDateTime()
    {
        $tokenTtl = $this->container->getParameter('lexik_jwt_authentication.token_ttl');
        $now = new \DateTime();
        $now->add(new \DateInterval('PT'.$tokenTtl.'S'));

        return $now->format('U');
    }

}