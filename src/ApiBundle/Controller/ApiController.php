<?php


namespace ApiBundle\Controller;

use AppBundle\Entity\Note;
use AppBundle\Entity\Institution;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use AppBundle\Entity\Categorie;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\QueryBuilder;

class ApiController extends Controller
{


    // Evenements
    /**
     * @Rest\View()
     * @Rest\Get("/api/events")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne tous les événements de livin dkr"
     * )
     */
    public function getEvents() {
        $events = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Event')
            ->findBy(
                array('status' => true),
                array('id' => 'DESC')
            );

        return $events;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/events")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne un événement de livin dkr suivant son identifiant"
     * )
     */
    public function getEventsById($id) {
        $event = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Event')
            ->find($id);

        return $event;
    }


    // Articles

    /**
     * @Rest\View()
     * @Rest\Get("/api/articles")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne tous les articles de livin dkr"
     * )
     */
    public function getArticles() {
        $articles = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Article')
            ->findBy(
                array('status' => true),
                array('id' => 'DESC')
            );

        return $articles;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/articles")
     * @ApiDoc(
     *  resource=true,
     *  description="Retourne un article de livin dkr suivant son identifiant"
     * )
     */
    public function getArticlesById($id) {
        $article = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Article')
            ->find($id);

        return $article;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/nb-articles")
     * @ApiDoc(
     *  resource=true,
     *  description="Incrémentation nombre de lecture"
     * )
     */
    public function nbArticlesAction($id) {

        $em = $this->getDoctrine()->getManager() ;


        $article = $em
            ->getRepository('AppBundle:Article')
            ->find($id);
        $nb = $article->getNbLecteur() + 1 ;

        $article->setNbLecteur($nb) ;

        $em->persist($article);
        $em->flush();

        $formatted = [
            "success" => "success",
            "nom_de_lecture" => $article->getNbLecteur()
        ];

        return $formatted ;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/like")
     * @ApiDoc(
     *  resource=true,
     *  description="Incrémentation nombre de like"
     * )
     */
    public function likeAction($id) {

        $em = $this->getDoctrine()->getManager() ;

        $article = $em
            ->getRepository('AppBundle:Article')
            ->find($id);

        if($article->getLike())
        {
            $article->setLike((integer) ($article->getLike() + 1) );
        }else{
            $article->setLike(1);
        }

        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $this->getUser()->getUsername()]);
        $user->setLike(true);

        $em->persist($article);
        $em->persist($user);
        $em->flush();

        $formatted = [
            "success" => "success",
            "like" => $article->getLike()
        ];

        return $formatted ;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/unlike")
     * @ApiDoc(
     *  resource=true,
     *  description="Incrémentation nombre de unlike"
     * )
     */
    public function unlikeAction($id) {

        $em = $this->getDoctrine()->getManager() ;

        $article = $em
            ->getRepository('AppBundle:Article')
            ->find($id);
        $nb = $article->getUnlike() + 1 ;

        $article->setUnlike($nb) ;


        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $this->getUser()->getUsername()]);
        $user->setLike(false);

        $em->persist($article);
        $em->persist($user);
        $em->flush();

        $formatted = [
            "success" => "success",
            "unlike" => $article->getUnlike()
        ];

        return $formatted ;
    }



    // Avis d'un etablissement

    /**
     * @Rest\View()
     * @Rest\Get("/api/{username}/user/avis")
     * @ApiDoc(
     *  resource=true,
     *  description="L'ensemble des avis d'un utilisateur"
     * )
     */
    public function getAvisUser($username) {

        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]);


        $avis = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Note')
            ->findBy(
                array('user' => $user),
                array('id' => 'DESC')
            );

        $formatted = [];
        foreach ($avis as $value) {
            if($value->getInstitution()) {
                $formatted [] = [
                    'id' => $value->getId(),
                    'avis' => $value->getAvis(),
                    'note' => $value->getNote(),
                    'user' => $value->getUser()->getUsername(),
                    'etablissement' => $value->getInstitution()->getNom()
                ];
            }elseif ($value->getEvent()){
                // 'etablissement' => $value->getEvent()->getNom(),
                $formatted [] = [
                    'id' => $value->getId(),
                    'avis' => $value->getAvis(),
                    'note' => $value->getNote(),
                    'user' => $value->getUser()->getUsername(),
                    'etablissement' => $value->getEvent()->getNom()
                ];
            }else{
                $formatted [] = [
                    'id' => $value->getId(),
                    'avis' => $value->getAvis(),
                    'note' => $value->getNote(),
                    'user' => $value->getUser()->getUsername()
                ];
            }
        }

        return $formatted;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/etablissement/avis")
     * @ApiDoc(
     *  resource=true,
     *  description="L'ensemble des avis d'un établissement"
     * )
     */

    public function getAvisEtablissement($id) {

        $etablissement = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Institution')
            ->find($id);


        $avis = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Note')
            ->findBy(
                array('institution' => $etablissement, 'status' => true),
                array('id' => 'DESC')
            );

        $formatted = [];
        foreach ($avis as $value) {
            $formatted [] = [
                'id' => $value->getId(),
                'avis' => $value->getAvis(),
                'note' => $value->getNote(),
                'user' => $value->getUser()->getUsername()
            ] ;
        }

        return $formatted;

    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/event/avis")
     * @ApiDoc(
     *  resource=true,
     *  description="L'ensemble des avis d'un événement"
     * )
     */
    public function getAvisEvent($id) {

        $event = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Event')
            ->find($id);


        $avis = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Note')
            ->findBy(
                array('event' => $event, 'status' => true),
                array('id' => 'DESC')
            );

        $formatted = [];
        foreach ($avis as $value) {
            $formatted [] = [
                'id' => $value->getId(),
                'avis' => $value->getAvis(),
                'note' => $value->getNote(),
                'user' => $value->getUser()->getUsername(),
            ] ;
        }

        return $formatted;

    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/api/{id}/etablissement/avis")
     *  @ApiDoc(
     *  description="Ajouter un avis pour un établissement",
     *  input="AppBundle\Form\NoteType"
     * )
     */
    public function postAvisEtablissement(Request $request, $id)
    {

        $em = $this->get('doctrine.orm.entity_manager');

        $institution = $em->getRepository('AppBundle:Institution')->find($id) ;

        $avis_user_to_institution = $institution->getAvis();

        foreach ($avis_user_to_institution as $value){
            if($value['username'] == $this->getUser()->getUsername()){

                $formatted [] = [
                    "message" => "Vous avez déjà posté un avis sur cet établissement."
                ] ;

                return $formatted;
            }
        }


        $avis = new Note();
        $avis->setAvis($request->get('avis'))
            ->setNote($request->get('note'))
            ->setStatus(true)
            ->setUser($this->getUser())
            ->setInstitution($institution);
        ;
        $em->persist($avis);

        $em->flush();

        $formatted [] = [
            'id' => $avis->getId(),
            'avis' => $avis->getAvis(),
            'note' => $avis->getNote(),
            'user' => $avis->getUser()->getUsername()
        ] ;

        return $formatted;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/api/{id}/event/avis")
     *  @ApiDoc(
     *  description="Ajouter un avis pour un événement",
     *  input="AppBundle\Form\NoteType"
     * )
     */
    public function postAvisEvent(Request $request, $id)
    {

        $em = $this->get('doctrine.orm.entity_manager');

        $event = $em->getRepository('AppBundle:Event')->find($id) ;
        $avis = new Note();
        $avis->setAvis($request->get('avis'))
            ->setNote($request->get('note'))
            ->setStatus(true)
            ->setUser($this->getUser())
            ->setEvent($event);
        ;
        $em->persist($avis);

        $em->flush();

        $formatted [] = [
            'id' => $avis->getId(),
            'avis' => $avis->getAvis(),
            'note' => $avis->getNote(),
            'user' => $avis->getUser()->getUsername()
        ] ;

        return $formatted;
    }



    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Put("/api/{id}/avis")
     *  @ApiDoc(
     *  description="Modifier un avis pour un établissement",
     *  input="AppBundle\Form\NoteType"
     * )
     */
    public function putAvis(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();

        $avis = $em->getRepository('AppBundle:Note')->find($id) ;

        if($request->get('avis'))
            $avis->setAvis($request->get('avis'));

        if($request->get('note'))
            $avis->setNote($request->get('note'));

        $em->persist($avis) ;
        $em->flush();

        return $avis ;
    }





    //Tags

    /**
     * @Rest\View()
     * @Rest\Get("/tags")
     * @ApiDoc(
     *  resource=true,
     *  description="Les tags pour découverte"
     * )
     */
    public function getAllTags() {
        $em = $this->get('doctrine.orm.entity_manager');

        $tags = $em->getRepository('AppBundle:TagDecouverte')->findAll();

        $formatted = [];

        foreach ($tags as $tag){
            $formatted [] = [
                "id" => $tag->getId(),
                "nom" => $tag->getNom(),
                "photo" => $tag->getPhoto(),
                "articles" => $tag->getArticles(),
                "events" => $tag->getEvents()
            ];
        }

        return $formatted;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{username}/tags")
     * @ApiDoc(
     *  resource=true,
     *  description="Tags d'un utilisateurs"
     * )
     */
    public function getAllTagsUser($username) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]);

        $tags = $em->getRepository('AppBundle:TagDecouverte')->findBy(['user' => $user]);

        $formatted = [];

        foreach ($tags as $tag){
            $formatted [] = [
                "id" => $tag->getId(),
                "nom" => $tag->getNom(),
                "photo" => $tag->getPhoto(),
            ];
        }

        return $formatted;
    }

    // Recommandation
    /**
     * @Rest\View()
     * @Rest\Post("/recommandations")
     * @ApiDoc(
     *  resource=true,
     *  description="Recommandation"
     * )
     */
    public function centreDinteretAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $username = $request->get('username');
        $tags = $request->get('tags');

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]);

        foreach ($user->getTags() as $tag) {
            $user->removeTag($tag) ;
        }

        $em->persist($user);

        $em->flush();



        foreach ($tags as $tag) {
            $tag_tmp = $em->getRepository('AppBundle:TagDecouverte')->find($tag);

            if($tag_tmp){
                $user->addTag($tag_tmp);
            }
        }


        $em->persist($user);

        $em->flush();


        $formatted = [
            "success" => "success"
        ];

        return $formatted;
    }

    /**
     * @Rest\View()
     * @Rest\Post("/remove/recommandations")
     * @ApiDoc(
     *  resource=true,
     *  description="Supprimer une recommandation"
     * )
     */
    public function removeCentreDinteretAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $username = $request->get('username');
        $tags = $request->get('tags');

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]);

        foreach ($tags as $tag) {
            $tag_tmp = $em->getRepository('AppBundle:TagDecouverte')->find($tag);
            if($tag_tmp)
                $user->removeTag($tag_tmp);
        }

        $em->persist($user);
        $em->flush();


        $formatted = [
            "success" => "success"
        ];

        return $formatted;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/etablissements/recommandations")
     * @ApiDoc(
     *  resource=true,
     *  description="Tags d'un utilisateurs"
     * )
     */
    public function recommandationEtablissementsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $recommandations_etab = $em
            ->getRepository('AppBundle:Institution')
            ->findBy(['promo' => true]);

        $formatted = [];
        foreach ($recommandations_etab as $etablissement) {
            if($etablissement->getCategorie()->getIdentifiant() == "restaurants") {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'menus' => $etablissement->getMenus(),
                    'avis' => $etablissement->getAvis(),
                    'offre' => $etablissement->getOffre(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),

                ] ;
            } else {
                $formatted [] = [
                    'id' => $etablissement->getId(),
                    'nom' => $etablissement->getNom(),
                    'description' => $etablissement->getDescription(),
                    'adresse' => $etablissement->getAdresse(),
                    'telephone' => $etablissement->getTelephone(),
                    'prix' => $etablissement->getPrice(),
                    'email' => $etablissement->getEmail(),
                    'facebook' => $etablissement->getFacebook(),
                    'twitter' => $etablissement->getTwitter(),
                    'instagram' => $etablissement->getInstagram(),
                    'galeries' => $etablissement->getGaleries(),
                    'user' => $etablissement->getUser()->getUsername(),
                    'categorie' => $etablissement->getCategorie(),
                    'promo' => $etablissement->getPromo(),
                    'avis' => $etablissement->getAvis(),
                    'offre' => $etablissement->getOffre(),
                    'longitude' => $etablissement->getLongitude(),
                    'latitude' => $etablissement->getLatitude(),

                ] ;
            }
        }

        return $formatted;
    }


    // Recommandation
    /**
     * @Rest\View()
     * @Rest\Get("/api/recommandation/articles/events")
     * @ApiDoc(
     *  resource=true,
     *  description="Recommandation d'articles et d'events suivant les centres d'intérêts"
     * )
     */
    public function recommandationArticlesEventsAction()
    {
        $tags = $this->getUser()->getTagsFormatted();

        $events = array();


        foreach ($tags as $tag) {
            foreach ( $tag["events"] as $event ) {
                if(!in_array($event, $events))
                    array_push($events, $event) ;
            }
        }

        $formatted = [
            "events" => $events
        ];

        return $formatted ;

    }



    // Set poids
    /**
     * @Rest\View()
     * @Rest\Post("/api/{username}/poids")
     * @ApiDoc(
     *  resource=true,
     *  description="Incrémentation nombre de lecture"
     * )
     */
    public function poidsAction($username) {

        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]);

        $nombre = $user->getPoids() + 1 ;
        $user->setPoids($nombre) ;

        $em->persist($user) ;
        $em->flush();

        $formatted = [
            "success" => "success"
        ];

        return $formatted ;
    }

    // Get users from poids
    /**
     * @Rest\View()
     * @Rest\Get("/api/poids")
     * @ApiDoc(
     *  resource=true,
     *  description="Incrémentation nombre de lecture"
     * )
     */
    public function getUsersPoidsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em
            ->getRepository('AppBundle:User')
            ->findBy([], array('poids' => 'DESC'), 100, null);

        $formatted = [] ;
        $position = 1;
        foreach ($users as $user) {
            if($user->getPoids() >= 10) {
                $formatted [] = [
                    "position" => $position,
                    "username" => $user->getUsername(),
                    "poids" => $user->getPoids()
                ] ;
            }

            $position++;
        }

        return $formatted ;
    }



    // Nous contacter
    /**
     * @Rest\View()
     * @Rest\Post("/api/contact")
     * @ApiDoc(
     *  resource=true,
     *  description="Nous contacter"
     * )
     */
    public function contactAction(Request $request) {

        $objet = $request->get('objet');
        $message = $request->get('message');

        $success = [
            "success" => "Votre message a été bien envoyé !"
        ];

        //envoie de mail
        $message = (new \Swift_Message('Formulaire de contact !'))
            ->setFrom('contact@livindkr.com')
            ->setTo('contact@livindkr.com')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'Emails/contact_mobile.html.twig',
                    array('objet' => $objet, 'message' => $message, 'user' => $this->getUser())
                ),
                'text/html'
            )
        ;

        $this->get('mailer')->send($message);

        return $success;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/etablissements/format-horaires")
     * @ApiDoc(
     *  resource=true,
     *  description="Formattage des horraires"
     * )
     */
    public function formatHorairesAction($id) {

        $em = $this->getDoctrine()->getManager() ;

        $horaire = $em->getRepository('AppBundle:Institution')->find($id)->getHoraires();

        $horaires = [] ;

        if ($horaire) {
            $formats = explode(PHP_EOL, $horaire);

            // dump(count($formats));
            // dump($formats);

            foreach ($formats as $value) {
                $format = explode('-', $value);
                if(count($format) == 1) {
                    $horaires [$this->getDay($format[0])] = [
                        "opening_time_one" => null,
                        "closing_time_one" => null,
                        "opening_time_two" => null,
                        "closing_time_two" => null,
                    ] ;
                } elseif (count($format) == 3) {
                    $horaires [$this->getDay($format[0])] = [
                        "opening_time_one" => $format[1],
                        "closing_time_one" => $format[2],
                        "opening_time_two" => null,
                        "closing_time_two" => null,
                    ] ;
                }
                elseif (count($format) == 5) {
                    $horaires [$this->getDay($format[0])] = [
                        "opening_time_one" => $format[1],
                        "closing_time_one" => $format[2],
                        "opening_time_two" => $format[3],
                        "closing_time_two" => $format[4],
                    ] ;
                }
            }
        }

        return new JsonResponse($horaire) ;

    }

    public function getDay($indice){
        if($indice == "L")
            return "Lundi" ;
        if($indice == "M")
            return "Mardi" ;
        if($indice == "MC")
            return "Mercredi" ;
        if($indice == "J")
            return "Jeudi" ;
        if($indice == "V")
            return "Vendredi" ;
        if($indice == "S")
            return "Samedi" ;
        if($indice == "D")
            return "Dimanche" ;
    }
}