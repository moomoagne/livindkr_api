<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 05/09/2018
 * Time: 17:36
 */

namespace ApiBundle\Controller;

use AppBundle\Entity\Note;
use AppBundle\Entity\Institution;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest; // alias pour toutes les annotations
use AppBundle\Entity\Categorie;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\QueryBuilder;

class CategorieController extends Controller
{
    // Catégories

    /**
     * @Rest\View()
     * @Rest\Get("/api/categories")
     * @ApiDoc(
     *  resource=true,
     *  description="Tous les catégories"
     * )
     */
    public function getAllCategorie(Request $request)
    {
        $categories = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Categorie')
            ->findAll();

        /* @var $categories Categorie[] */

        $formatted = [];
        foreach ($categories as $category) {
            $formatted [] = [
                'id' => $category->getId(),
                'nom' => $category->getNom(),
                'image' => $category->getImage(),
                'description' => $category->getDesription()
            ] ;
        }

        return $formatted;
    }

    // Sous categories
    /**
     * @Rest\View()
     * @Rest\Get("/api/{id}/categories/souscategories")
     * @ApiDoc(
     *  resource=true,
     *  description="Tous les sous catégories d'une catégorie"
     * )
     */
    public function getAllSousCategories(Request $request, $id)
    {
        $categorie = $this->getDoctrine()->getManager()->getRepository('AppBundle:Categorie')
            ->find($id);

        $sous_categories = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:SousCategorie')
            ->findBy(array('categorie' => $categorie));

        /* @var $categories Categorie[] */

        // $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $formatted = [];

        foreach ($sous_categories as $sous_category) {
            $formatted [] = [
                'id' => $sous_category->getId(),
                'nom' => $sous_category->getNom(),
                'image' => $sous_category->getImage(),
            ] ;
        }

        return $formatted;
    }

}