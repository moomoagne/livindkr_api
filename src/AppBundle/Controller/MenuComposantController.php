<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MenuComposant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Menucomposant controller.
 *
 * @Route("menucomposant")
 */
class MenuComposantController extends Controller
{
    /**
     * Lists all menuComposant entities.
     *
     * @Route("/", name="menucomposant_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $menuComposants = $em->getRepository('AppBundle:MenuComposant')->findAll();

        return $this->render('menucomposant/index.html.twig', array(
            'menuComposants' => $menuComposants,
        ));
    }

    /**
     * Creates a new menuComposant entity.
     *
     * @Route("/new/ajax/composant", name="menucomposant_ajax_new")
     * @Method({"GET", "POST"})
     */
    public function newAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $composant = new MenuComposant();
        $composant
            ->setTitre($request->get('titre'))
            ->setPrix($request->get('prix'))
            ->setVignette($request->get('photo'))
        ;

        /*
         * @var AppBundle\Menu
         */

        $menu = $em->getRepository('AppBundle:Menu')->find($request->get('menu'));
        $menu->addComposant($composant);
        $composant->setMenu($menu);


        $em->persist($menu);
        $em->persist($composant);

        $em->flush();

        $formatted = [
            'id' => $composant->getId(),
            'titre' => $composant->getTitre(),
            'prix' => $composant->getPrix(),
            'photo' => $composant->getVignette(),
            'menu' => $composant->getMenu()->getId()
        ] ;

        return new JsonResponse($formatted);
    }

    /**
     * Creates a new menuComposant entity.
     *
     * @Route("/new/{menu}", name="menucomposant_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $menu)
    {
        $menuComposant = new Menucomposant();
        $form = $this->createForm('AppBundle\Form\MenuComposantType', $menuComposant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // dump($menu); die ;

            if($menuComposant->getVignette()){

                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $menuComposant->getVignette();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $menuComposant->setVignette($fileName);

            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($menuComposant);
            $em->flush();

            return $this->redirectToRoute('menucomposant_show', array('id' => $menuComposant->getId()));
        }

        return $this->render('menucomposant/new.html.twig', array(
            'menuComposant' => $menuComposant,
            'form' => $form->createView(),
            'menu' => $menu
        ));
    }

    /**
     * Finds and displays a menuComposant entity.
     *
     * @Route("/{id}", name="menucomposant_show")
     * @Method("GET")
     */
    public function showAction(MenuComposant $menuComposant)
    {
        $deleteForm = $this->createDeleteForm($menuComposant);

        return $this->render('menucomposant/show.html.twig', array(
            'menuComposant' => $menuComposant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing menuComposant entity.
     *
     * @Route("/{id}/edit", name="menucomposant_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MenuComposant $menuComposant)
    {

        $filename = null;

        if ($menuComposant->getVignette())
        {
            $filename = $menuComposant->getVignette();
            $menuComposant->setVignette(new File($this->getParameter('articles_directory'). '/'. $menuComposant->getVignette()));
        }

        $deleteForm = $this->createDeleteForm($menuComposant);
        $editForm = $this->createForm('AppBundle\Form\MenuComposantType', $menuComposant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($filename != null) {
                $menuComposant->setVignette($filename) ;
            }

            if($menuComposant->getVignette() && $menuComposant->getVignette() != $filename){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $menuComposant->getVignette();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $menuComposant->setVignette($fileName);
            }

            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('menucomposant_edit', array('id' => $menuComposant->getId()));
            return $this->redirect($request->getReferer());

        }

        return $this->render('menucomposant/edit.html.twig', array(
            'menuComposant' => $menuComposant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a menuComposant entity.
     *
     * @Route("/{id}", name="menucomposant_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MenuComposant $menuComposant)
    {
        $form = $this->createDeleteForm($menuComposant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($menuComposant);
            $em->flush();
        }

        return $this->redirectToRoute('menucomposant_index');
    }

    /**
     * Creates a form to delete a menuComposant entity.
     *
     * @param MenuComposant $menuComposant The menuComposant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MenuComposant $menuComposant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('menucomposant_delete', array('id' => $menuComposant->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
