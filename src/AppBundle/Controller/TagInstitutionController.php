<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TagInstitution;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Taginstitution controller.
 *
 * @Route("admin/etiquettes")
 */
class TagInstitutionController extends Controller
{
    /**
     * Lists all tagInstitution entities.
     *
     * @Route("/", name="taginstitution_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tagInstitutions = $em->getRepository('AppBundle:TagInstitution')->findAll();

        return $this->render('taginstitution/index.html.twig', array(
            'tagInstitutions' => $tagInstitutions,
        ));
    }

    /**
     * Creates a new tagInstitution entity.
     *
     * @Route("/new", name="taginstitution_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tagInstitution = new Taginstitution();
        $form = $this->createForm('AppBundle\Form\TagInstitutionType', $tagInstitution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($tagInstitution->getPhoto()){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $tagInstitution->getPhoto();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $tagInstitution->setPhoto($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($tagInstitution);
            $em->flush();

            return $this->redirectToRoute('taginstitution_index');
        }

        return $this->render('taginstitution/new.html.twig', array(
            'tagInstitution' => $tagInstitution,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tagInstitution entity.
     *
     * @Route("/{id}", name="taginstitution_show")
     * @Method("GET")
     */
    public function showAction(TagInstitution $tagInstitution)
    {
        $deleteForm = $this->createDeleteForm($tagInstitution);

        return $this->render('taginstitution/show.html.twig', array(
            'tagInstitution' => $tagInstitution,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tagInstitution entity.
     *
     * @Route("/{id}/edit", name="taginstitution_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TagInstitution $tagInstitution)
    {

        $filename = null;

        if ( $tagInstitution->getPhoto() )
        {
            $filename = $tagInstitution->getPhoto();
            $tagInstitution->setPhoto(
                new File($this->getParameter('articles_directory'). '/'. $tagInstitution->getPhoto())
            );
        }
        $deleteForm = $this->createDeleteForm($tagInstitution);
        $editForm = $this->createForm('AppBundle\Form\TagInstitutionType', $tagInstitution);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($filename != null) {
                $tagInstitution->setPhoto($filename) ;
            }

            if($tagInstitution->getPhoto() && $tagInstitution->getPhoto() != $filename){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $tagInstitution->getPhoto();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $tagInstitution->setPhoto($fileName);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('taginstitution_edit', array('id' => $tagInstitution->getId()));
        }

        return $this->render('taginstitution/edit.html.twig', array(
            'tagInstitution' => $tagInstitution,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tagInstitution entity.
     *
     * @Route("/{id}", name="taginstitution_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TagInstitution $tagInstitution)
    {
        $form = $this->createDeleteForm($tagInstitution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tagInstitution);
            $em->flush();
        }

        return $this->redirectToRoute('taginstitution_index');
    }

    /**
     * Creates a form to delete a tagInstitution entity.
     *
     * @param TagInstitution $tagInstitution The tagInstitution entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TagInstitution $tagInstitution)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('taginstitution_delete', array('id' => $tagInstitution->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
