<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $institutions = $em->getRepository('AppBundle:Institution')->findBy(array('user'=>$this->getUser()));
        $categories = $em->getRepository('AppBundle:Categorie')->findBy(array("is_entity" => true));
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'institutions' => $institutions,
            'cats' => $categories
        ));
    }

    /**
     * @Route("/ajax/subcategories", name="ajax_sub_categories")
     */
    public function ajaxSubCategorieAction(Request $request)
    {
        if($request->get('id')) {
            $em = $this->getDoctrine()->getManager();
            $category = $em->getRepository('AppBundle:Categorie')->find($request->get('id'));
            $subCategories = $em->getRepository('AppBundle:SousCategorie')->findBy(array('categorie' => $category));

            $formatted = [];

            foreach ($subCategories as $value) {
                $formatted [] = [
                    'id' => $value->getId(),
                    'nom' => $value->getNom()
                ];
            }

            return new JsonResponse($formatted);
        }

        return new JsonResponse('error');
    }
}
