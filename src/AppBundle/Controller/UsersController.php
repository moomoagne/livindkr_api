<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UsersController extends Controller
{
    /**
     * @Route("/admin/users", name="user_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $role = 'ROLE_ADMIN' ;

        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');
        $admins= $qb->getQuery()->getResult() ;

        $role = 'ROLE_USER' ;
        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');

        $clients= $qb->getQuery()->getResult() ;

        return $this->render('AppBundle:Users:index.html.twig', array(
            'clients' => $clients,
            'admins' => $admins
        ));
    }

    # Activate or desactivate user (client_activate)
    /**
     * @Route("/admin/enabled/{username}/{status}", name="client_activate")
     */
    public function enabledAction($username, $status)
    {
        $em = $this->getDoctrine()->getManager();

        /*
         * @var AppBundle\User
         */
        $user = $em->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]) ;

        // Retrieve flashbag from the controller
        $flashbag = $this->get('session')->getFlashBag();

        if($status == 1) {
            $user->setEnabled(true) ;
            // Add flash message
            $flashbag->add("success", "Le compte d'utilisateur '$username' à été activé");
        }

        if($status == 0) {
            $user->setEnabled(false) ;
            // Add flash message
            $flashbag->add("failed", "Le compte d'utilisateur '$username' à été désactivé");
        }

        $em->persist($user) ;
        $em->flush();


        return $this->redirectToRoute('user_index') ;
    }

}
