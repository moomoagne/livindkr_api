<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Event controller.
 *
 * @Route("event")
 */
class EventController extends Controller
{
    /**
     * Lists all event entities.
     *
     * @Route("/approve/{id}/{status}", name="event_approve")
     * @Method("GET")
     */
    public function approveAction($id, $status)
    {
        $em = $this->getDoctrine()->getManager();

        $event = $em->getRepository('AppBundle:Event')->find($id);

        if($status == 1)
        {
            $event->setStatus(1);

            //send mail

            if($event) {
                $message = (new \Swift_Message('Salut '))
                    ->setFrom('contact@livindkr.com')
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/event_approve_success.html.twig',
                            array('event' => $event)
                        ),
                        'text/html'
                    )
                    /*
                     * If you also want to include a plaintext version of the message
                    ->addPart(
                        $this->renderView(
                            'Emails/registration.txt.twig',
                            array('name' => $name)
                        ),
                        'text/plain'
                    )
                    */
                ;


                $this->get('mailer')->send($message);
            }
            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("success_approve", "Evénement approuvé avec succès !");

        }elseif ($status == 0)
        {

            //send mail

            if($event) {
                $message = (new \Swift_Message('Salut '))
                    ->setFrom('contact@livindkr.com')
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/event_approve_failed.html.twig',
                            array('event' => $event)
                        ),
                        'text/html'
                    )
                    /*
                     * If you also want to include a plaintext version of the message
                    ->addPart(
                        $this->renderView(
                            'Emails/registration.txt.twig',
                            array('name' => $name)
                        ),
                        'text/plain'
                    )
                    */
                ;


                $this->get('mailer')->send($message);
            }

            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("success_approve", "Evénement non approuvé !");

            $event->setStatus(0);
        }

        $em->persist($event);
        $em->flush();

        return $this->redirectToRoute('event_index');
    }

    /**
     * Lists all event entities.
     *
     * @Route("/", name="event_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('AppBundle:Event')->findBy(array('user'=>$this->getUser()));

        if(in_array('ROLE_SUPER_ADMIN',$this->getUser()->getRoles()))
        {
            $events = $em->getRepository('AppBundle:Event')->findAll();
        }

        return $this->render('event/index.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * Creates a new event entity.
     *
     * @Route("/new", name="event_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $event = new Event();
        $form = $this->createForm('AppBundle\Form\EventType', $event);
        $form->handleRequest($request);

        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $event->setUser($user);

            if(in_array('ROLE_ADMIN',$user->getRoles()))
            {
                $event->setStatus(2);
            }else{
                $event->setStatus(1);
            }

            if($event->getPhoto()){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $event->getPhoto();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $event->setPhoto($fileName);
            }

            $em->persist($event);
            $em->flush();

            $em->flush($event);


            //send mail

            $message = (new \Swift_Message('Salut '+ $this->getUser()->getUsername()))
                ->setFrom('contact@livindkr.com')
                ->setTo($this->getUser()->getEmail())
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'Emails/event_approve.html.twig',
                        array('event' => $event)
                    ),
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;


            $this->get('mailer')->send($message);

            return $this->redirectToRoute('event_show', array('id' => $event->getId()));
        }

        return $this->render('event/new.html.twig', array(
            'event' => $event,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a event entity.
     *
     * @Route("/{id}", name="event_show")
     * @Method("GET")
     */
    public function showAction(Event $event)
    {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('event/show.html.twig', array(
            'event' => $event,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing event entity.
     *
     * @Route("/{id}/edit", name="event_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Event $event)
    {
        $filename = null;

        if ($event->getPhoto())
        {
            $filename = $event->getPhoto();
            $event->setPhoto(
                new File($this->getParameter('articles_directory'). '/'. $event->getPhoto())
            );
        }
        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm('AppBundle\Form\EventType', $event);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($filename != null) {
                $event->setPhoto($filename) ;
            }

            if($event->getPhoto() && $event->getPhoto() != $filename){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $event->getPhoto();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $event->setPhoto($fileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('event_show', array('id' => $event->getId()));
        }

        return $this->render('event/edit.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a event entity.
     *
     * @Route("/{id}", name="event_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Event $event)
    {
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('event_index');
    }

    /**
     * Creates a form to delete a event entity.
     *
     * @param Event $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
