<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EventPhoto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Eventphoto controller.
 *
 * @Route("eventphoto")
 */
class EventPhotoController extends Controller
{
    /**
     * Lists all eventPhoto entities.
     *
     * @Route("/", name="eventphoto_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $eventPhotos = $em->getRepository('AppBundle:EventPhoto')->findAll();

        return $this->render('eventphoto/index.html.twig', array(
            'eventPhotos' => $eventPhotos,
        ));
    }

    /**
     * Creates a new eventPhoto entity.
     *
     * @Route("/new", name="eventphoto_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $eventPhoto = new Eventphoto();
        $form = $this->createForm('AppBundle\Form\EventPhotoType', $eventPhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($eventPhoto);
            $em->flush();

            return $this->redirectToRoute('eventphoto_show', array('id' => $eventPhoto->getId()));
        }

        return $this->render('eventphoto/new.html.twig', array(
            'eventPhoto' => $eventPhoto,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a eventPhoto entity.
     *
     * @Route("/{id}", name="eventphoto_show")
     * @Method("GET")
     */
    public function showAction(EventPhoto $eventPhoto)
    {
        $deleteForm = $this->createDeleteForm($eventPhoto);

        return $this->render('eventphoto/show.html.twig', array(
            'eventPhoto' => $eventPhoto,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing eventPhoto entity.
     *
     * @Route("/{id}/edit", name="eventphoto_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, EventPhoto $eventPhoto)
    {
        $deleteForm = $this->createDeleteForm($eventPhoto);
        $editForm = $this->createForm('AppBundle\Form\EventPhotoType', $eventPhoto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('eventphoto_edit', array('id' => $eventPhoto->getId()));
        }

        return $this->render('eventphoto/edit.html.twig', array(
            'eventPhoto' => $eventPhoto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a eventPhoto entity.
     *
     * @Route("/{id}", name="eventphoto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, EventPhoto $eventPhoto)
    {
        $form = $this->createDeleteForm($eventPhoto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($eventPhoto);
            $em->flush();
        }

        return $this->redirectToRoute('eventphoto_index');
    }

    /**
     * Creates a form to delete a eventPhoto entity.
     *
     * @param EventPhoto $eventPhoto The eventPhoto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EventPhoto $eventPhoto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('eventphoto_delete', array('id' => $eventPhoto->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
