<?php

namespace AppBundle\Form;

use Bnbc\UploadBundle\Form\Type\AjaxfileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BienEtreType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
            ->add('description')
            ->add('adresse')
            ->add('telephone')
            ->add('price', ChoiceType::class, array(
                'choices'  => array(
                    '$' => '$',
                    '$$' => '$$',
                    '$$$' => '$$$',
                    '$$$$' => '$$$$',
                ),
            ))
            ->add('promo')
            ->add('longitude')
            ->add('latitude')
            ->add('photo', FileType::class, array(
                "data_class" => null
            ))
            ->add('offre')
            ->add('categorie')
            ->add('sousCategorie')
            ->add('promo');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BienEtre'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bienetre';
    }


}
