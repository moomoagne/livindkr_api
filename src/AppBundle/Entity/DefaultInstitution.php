<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DefaultInstitution
 *
 * @ORM\Table(name="default_institution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DefaultInstitutionRepository")
 */
class DefaultInstitution extends Institution
{

}
