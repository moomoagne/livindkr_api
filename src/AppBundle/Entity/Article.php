<?php

namespace AppBundle\Entity;

use AppBundle\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity
 */
class Article extends ArticleRepository
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text
     *
     * @ORM\Column(name="contenu", type="text", nullable=true)
     */
    private $contenu;

    /**
     * @var datetime
     *
     * @ORM\Column(name="date_article", type="datetime", nullable=true)
     */
    private $dateArticle;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/jpg","image/jpeg","image/png" })
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_lecteur", type="integer", nullable=true)
     */
    private $nbLecteur;

    /**
     * @var integer
     *
     * @ORM\Column(name="likes", type="integer", nullable=true)
     */
    private $like;

    /**
     * @var integer
     *
     * @ORM\Column(name="unlikes", type="integer", nullable=true)
     */
    private $unlike;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_auteur", type="string", length=255, nullable=true)
     */
    private $nomAuteur;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_blog", type="string", length=255, nullable=true)
     */
    private $lienBlog;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_category", referencedColumnName="id", nullable=true)
     * })
     */
    private $categorie;

    /**
     * Many User have Many Tags.
     * @ORM\ManyToMany(targetEntity="TagDecouverte", inversedBy="articles", orphanRemoval=true)
     * @ORM\JoinTable(name="article_tags",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", unique=true, nullable=true)}
     *      )
     */
    private $tags;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status; // 1 = A, 0 = R, 2 = En cours

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Article
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Article
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set dateArticle
     *
     * @param string $dateArticle
     *
     * @return Article
     */
    public function setDateArticle($dateArticle)
    {
        $this->dateArticle = $dateArticle;

        return $this;
    }

    /**
     * Get dateArticle
     *
     * @return string
     */
    public function getDateArticle()
    {
        return $this->dateArticle;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set nbLecteur
     *
     * @param integer $nbLecteur
     *
     * @return Article
     */
    public function setNbLecteur($nbLecteur)
    {
        $this->nbLecteur = $nbLecteur;

        return $this;
    }

    /**
     * Get nbLecteur
     *
     * @return integer
     */
    public function getNbLecteur()
    {
        return $this->nbLecteur;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Article
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Article
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\Categorie $categorie
     *
     * @return Article
     */
    public function setCategorie(\AppBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateArticle = new \DateTime('now');
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\TagDecouverte $tag
     *
     * @return Article
     */
    public function addTag(\AppBundle\Entity\TagDecouverte $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\TagDecouverte $tag
     */
    public function removeTag(\AppBundle\Entity\TagDecouverte $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdTags()
    {
        $tags= array();

        foreach ($this->tags as $tag){
            array_push($tags, $tag->getId());
        }
        return $tags;
    }



    /**
     * Set nomAuteur
     *
     * @param string $nomAuteur
     *
     * @return Article
     */
    public function setNomAuteur($nomAuteur)
    {
        $this->nomAuteur = $nomAuteur;

        return $this;
    }

    /**
     * Get nomAuteur
     *
     * @return string
     */
    public function getNomAuteur()
    {
        return $this->nomAuteur;
    }

    /**
     * Set lienBlog
     *
     * @param string $lienBlog
     *
     * @return Article
     */
    public function setLienBlog($lienBlog)
    {
        $this->lienBlog = $lienBlog;

        return $this;
    }

    /**
     * Get lienBlog
     *
     * @return string
     */
    public function getLienBlog()
    {
        return $this->lienBlog;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return Article
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return Article
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return Article
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Set like
     *
     * @param integer $like
     *
     * @return Article
     */
    public function setLike($like)
    {
        $this->like = $like;

        return $this;
    }

    /**
     * Get like
     *
     * @return integer
     */
    public function getLike()
    {
        return $this->like;
    }

    /**
     * Set unlike
     *
     * @param integer $unlike
     *
     * @return Article
     */
    public function setUnlike($unlike)
    {
        $this->unlike = $unlike;

        return $this;
    }

    /**
     * Get unlike
     *
     * @return integer
     */
    public function getUnlike()
    {
        return $this->unlike;
    }
}
