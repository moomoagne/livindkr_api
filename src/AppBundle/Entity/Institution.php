<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Institution
 *
 * @ORM\Table(name="institution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InstitutionRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap(
 *     {
 *          "restaurants": "AppBundle\Entity\Restaurants",
 *          "shopping": "AppBundle\Entity\Shopping",
 *          "bien_etre": "AppBundle\Entity\BienEtre",
 *          "night_life": "AppBundle\Entity\NightLife",
 *          "mode_beaute": "AppBundle\Entity\ModeBeaute",
 *          "things_to_do": "AppBundle\Entity\ThingsToDo",
 *          "bien_etre": "AppBundle\Entity\BienEtre",
 *          "prestataires": "AppBundle\Entity\Prestataires",
 *          "hotels": "AppBundle\Entity\Hotels",
 *          "default": "AppBundle\Entity\DefaultInstitution",
 *      })
 */
abstract class Institution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255, nullable=true)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg", "image/jpg" , "image/png" })
     *
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="horaires", type="text", nullable=true)
     */
    private $horaires;

    /**
     * @var string
     *
     * @ORM\Column(name="siteweb", type="string", length=255, nullable=true)
     */
    private $siteweb;


    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=1000, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=1000, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", length=1000, nullable=true)
     */
    private $instagram;


    /**
     * Many User have Many Galeries.
     * @ORM\ManyToMany(targetEntity="Galerie")
     * @ORM\JoinTable(name="institution_galeries",
     *      joinColumns={@ORM\JoinColumn(name="institution_id", referencedColumnName="id", nullable=true)},
     *      inverseJoinColumns={@ORM\JoinColumn(name="galerie_id", referencedColumnName="id", unique=true, nullable=true)}
     *      )
     */
    private $galeries;


    /**
     * @ORM\ManyToOne(targetEntity="Offre", inversedBy="institutions")
     * @ORM\JoinColumn(name="offre_id", referencedColumnName="id", nullable=false)
     */
    private $offre;


    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * @var \AppBundle\Entity\Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_category", referencedColumnName="id")
     * })
     */
    private $categorie;

    /**
     * @var \AppBundle\Entity\SousCategorie
     *
     * @ORM\ManyToOne(targetEntity="SousCategorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sous_category", referencedColumnName="id", nullable=true)
     * })
     */
    private $sousCategorie;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status; // 1 = A, 0 = R, 2 = En cours


    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="TagInstitution", mappedBy="etablissement", cascade={"persist", "remove"})
     */
    private $etiquettes;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="Note", mappedBy="institution")
     */
    private $avis;

    /**
     * @var string
     *
     * @ORM\Column(name="specialite", type="string", length=1000, nullable=true)
     */
    private $specialite;

    /**
     * @return string
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * @param string $specialite
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;
    }



    public function __construct()
    {
        $this->galeries = new \Doctrine\Common\Collections\ArrayCollection();
        // $this->menus = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Institution
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Institution
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Institution
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Institution
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Institution
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Institution
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Institution
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Institution
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Add galery
     *
     * @param \AppBundle\Entity\Galerie $galery
     *
     * @return Institution
     */
    public function addGalery(\AppBundle\Entity\Galerie $galery)
    {
        $this->galeries[] = $galery;

        return $this;
    }

    /**
     * Remove galery
     *
     * @param \AppBundle\Entity\Galerie $galery
     */
    public function removeGalery(\AppBundle\Entity\Galerie $galery)
    {
        $this->galeries->removeElement($galery);
    }

    /**
     * Get galeries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGaleries()
    {
        return $this->galeries;
    }

    /**
     * Set offre
     *
     * @param \AppBundle\Entity\Offre $offre
     *
     * @return Institution
     */
    public function setOffre(\AppBundle\Entity\Offre $offre = null)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return \AppBundle\Entity\Offre
     */
    public function getOffre()
    {
        $offre = $this->offre;
        $formatted = [] ;
        if($offre){
            $formatted= [
                "nom" => $offre->getNom(),
                "identifiant" => $offre->getIdentifiant()
            ] ;
        }
        return $formatted ;
    }

    /**
     * Get offre
     *
     * @return \AppBundle\Entity\Offre
     */
    public function getOneOffre(){

        return $this->offre;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\Categorie $categorie
     *
     * @return Institution
     */
    public function setCategorie(\AppBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\Categorie
     */
    public function getCategorieFomatted()
    {
        $categorie = $this->categorie ;
        $formatted = [
            "id" => $categorie->getId(),
            "nom" => $categorie->getNom(),
            "image" => $categorie->getImage(),
            "description" => $categorie->getDesription()
        ] ;
        return $formatted;
    }

    /**
     * @var bool
     *
     * @ORM\Column(name="promo", type="boolean", nullable=true)
     */
    private $promo;

    /**
     * Set promo
     *
     * @param boolean $promo
     *
     * @return Restaurants
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return bool
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Institution
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return Institution
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return Institution
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return Institution
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Institution
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set siteweb
     *
     * @param string $siteweb
     *
     * @return Institution
     */
    public function setSiteweb($siteweb)
    {
        $this->siteweb = $siteweb;

        return $this;
    }

    /**
     * Get siteweb
     *
     * @return string
     */
    public function getSiteweb()
    {
        return $this->siteweb;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Institution
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sousCategorie
     *
     * @param \AppBundle\Entity\SousCategorie $sousCategorie
     *
     * @return Institution
     */
    public function setSousCategorie(\AppBundle\Entity\SousCategorie $sousCategorie = null)
    {
        $this->sousCategorie = $sousCategorie;

        return $this;
    }

    /**
     * Get sousCategorie
     *
     * @return \AppBundle\Entity\SousCategorie
     */
    public function getSousCategorie()
    {
        return $this->sousCategorie;
    }

    /**
     * Add avi
     *
     * @param \AppBundle\Entity\Note $avi
     *
     * @return Institution
     */
    public function addAvi(\AppBundle\Entity\Note $avi)
    {
        $this->avis[] = $avi;

        return $this;
    }

    /**
     * Remove avi
     *
     * @param \AppBundle\Entity\Note $avi
     */
    public function removeAvi(\AppBundle\Entity\Note $avi)
    {
        $this->avis->removeElement($avi);
    }

    /**
     * Get avis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvis()
    {
        $avis = $this->avis;
        $formatted = [] ;
        foreach ($avis as $avi) {
            $formatted [] = [
                "id" => $avi->getId(),
                "note" => $avi->getNote(),
                "avis" => $avi->getAvis(),
                "username" => $avi->getUser()->getUsername()
            ] ;
        }
        return $formatted;
    }


    /**
     * Add etiquette
     *
     * @param \AppBundle\Entity\TagInstitution $etiquette
     *
     * @return Institution
     */
    public function addEtiquette(\AppBundle\Entity\TagInstitution $etiquette)
    {
        $this->etiquettes[] = $etiquette;

        return $this;
    }

    /**
     * Remove etiquette
     *
     * @param \AppBundle\Entity\TagInstitution $etiquette
     */
    public function removeEtiquette(\AppBundle\Entity\TagInstitution $etiquette)
    {
        $this->etiquettes->removeElement($etiquette);
    }

    /**
     * Get etiquettes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtiquettes()
    {
        return $this->etiquettes;
    }


    /**
     * Set email
     *
     * @param string $horaires
     *
     * @return Institution
     */
    public function setHoraires($horaires)
    {
        $this->horaires = $horaires;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getHoraires()
    {
        return $this->horaires;
    }
}
