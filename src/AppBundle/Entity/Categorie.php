<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="desription", type="text", nullable=true)
     */
    private $desription;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_entity", type="boolean", nullable=true)
     */
    private $is_entity;

    /**
     * @var string
     *
     * @ORM\Column(name="identifiant", type="string", length=255, nullable=true)
     */
    private $identifiant;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $ordre;


    /**
     * @ORM\OneToMany(targetEntity="SousCategorie", mappedBy="categorie")
     */
    private $sousCategories;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Categorie
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set desription
     *
     * @param string $desription
     *
     * @return Categorie
     */
    public function setDesription($desription)
    {
        $this->desription = $desription;

        return $this;
    }

    /**
     * Get desription
     *
     * @return string
     */
    public function getDesription()
    {
        return $this->desription;
    }

    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * Set isEntity
     *
     * @param boolean $isEntity
     *
     * @return Categorie
     */
    public function setIsEntity($isEntity)
    {
        $this->is_entity = $isEntity;

        return $this;
    }

    /**
     * Get isEntity
     *
     * @return boolean
     */
    public function getIsEntity()
    {
        return $this->is_entity;
    }

    

    /**
     * Set identifiant
     *
     * @param string $identifiant
     *
     * @return Categorie
     */
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    /**
     * Get identifiant
     *
     * @return string
     */
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     *
     * @return Categorie
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return integer
     */
    public function getOrdre()
    {
        return $this->ordre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sousCategories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sousCategory
     *
     * @param \AppBundle\Entity\SousCategorie $sousCategory
     *
     * @return Categorie
     */
    public function addSousCategory(\AppBundle\Entity\SousCategorie $sousCategory)
    {
        $this->sousCategories[] = $sousCategory;

        return $this;
    }

    /**
     * Remove sousCategory
     *
     * @param \AppBundle\Entity\SousCategorie $sousCategory
     */
    public function removeSousCategory(\AppBundle\Entity\SousCategorie $sousCategory)
    {
        $this->sousCategories->removeElement($sousCategory);
    }

    /**
     * Get sousCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSousCategories()
    {
        return $this->sousCategories;
    }
}
