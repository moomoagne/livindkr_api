<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TagInstitution
 *
 * @ORM\Table(name="tag_institution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagInstitutionRepository")
 */
class TagInstitution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Institution", inversedBy="tags_etab")
     * @ORM\JoinColumn(name="institution_id", referencedColumnName="id", nullable=true)
     */
    private $etablissement;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return TagInstitution
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return TagInstitution
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set etablissement
     *
     * @param \AppBundle\Entity\Institution $etablissement
     *
     * @return TagInstitution
     */
    public function setEtablissement(\AppBundle\Entity\Institution $etablissement = null)
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \AppBundle\Entity\Institution
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    public function __toString()
    {
        return $this->getNom();
    }
}
