<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersInterests
 *
 * @ORM\Table(name="interests_user")
 * @ORM\Entity
 */
class InterestsUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Interest
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Interest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_interest", referencedColumnName="id")
     * })
     */
    private $interest;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return InterestsUser
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set interest
     *
     * @param \AppBundle\Entity\Interest $interest
     *
     * @return InterestsUser
     */
    public function setInterest(\AppBundle\Entity\Interest $interest = null)
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * Get interest
     *
     * @return \AppBundle\Entity\Interest
     */
    public function getInterest()
    {
        return $this->interest;
    }
}
