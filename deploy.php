<?php
namespace Deployer;

require 'recipe/symfony3.php';

// Project name
set('application', 'livindkr');

// Project repository
set('repository', 'git@gitlab.qualshore.com:senol/livindkr_api.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', ['app/config/parameters.yml']);
add('shared_dirs', ['web/uploads', 'web/images']);

// Writable dirs by web server 
add('writable_dirs', ['web/uploads', 'web/images']);


// Hosts

host('livindkr.qualshore.com')
    ->stage('prod')
	->hostname('109.238.3.52')
	->set('application', 'livindkr')
    ->set('deploy_path', '/var/www/html/{{application}}')
    ->user('sys_admin')
    ->set('branch', 'master')
    ->set('http_user', 'www-data')
    ->set('env', [
    	'SYMFONY_ENV' => 'prod',
    	'SYMFONY__DATABASE__USER' => 'root',
    	'SYMFONY__DATABASE__PASSWORD' => 'B2Pohipr?&',
    	'SYMFONY__DATABASE__HOST' => '127.0.0.1',
    	'SYMFONY__DATABASE__PORT' => '3306',
    	'SYMFONY__DATABASE__NAME' => 'livindkr'
     ])
	->set('writable_mode', 'chmod')
    ->set('writable_use_sudo', false)
;    
	
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task('doctrine:schema:update', function() {
    run("cd {{release_path}} && {{bin/php}} app/console doctrine:schema:update --force --env=prod");
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'doctrine:schema:update');

